To get a final table you have to run 2 python codes:
==================================================== 
FIRST: a2256_table.py which match radio galaxies and all optical galaxies.

SECOND: a2256_table_final.py which match the list from FIRST part to list of SDSS(which have to get from matched FIRST list)

#################################### FIRST ####################################

INPUT for a2256_table.py:
=========================
NOTE: All inputs should be in a form of CSV files.

1. File that contanes radio properties of galaxies ==> "numbur, ra, dec, flux, peak_flux".

2. File that contanes rms values of image in the increasing format as the radiuse gets bigger. ==> "radiuse valuse from the imag , rms"

3. File that contanes optical properties of galaxies from NED ==> "numbur, ra, dec, redshift, magnitude"

*4. (Optional) File that contanes notes of specific objects with the number of them. ==> "number, note "

Result from a2256_table.py:
===========================

- It matches lists that you gave as the input and make some files in order to use if for second python code to match with SDSS list.

-It makes a txt file name "SDSS_Inp.txt" that you have to use this file to get other information from SDSS website as below :

go to this SDSS website and upload the SDSS_Inp.txt with the script below:

http://skyserver.sdss.org/dr10/en/tools/crossid/crossid.aspx

				+

SELECT 
   p.objID, p.ra, p.dec, p.run, p.rerun, p.camcol, p.field,
   dbo.fPhotoTypeN(p.type) as type,
   p.modelMag_u, p.modelMag_g, p.modelMag_r, p.modelMag_i, p.modelMag_z,p.fracDeV_u,p.fracDeV_g,p.fracDeV_r,p.fracDeV_i,p.fracDeV_z
FROM #upload u
      JOIN #x x ON x.up_id = u.up_id
      JOIN PhotoTag p ON p.objID = x.objID 
ORDER BY x.up_id
		


save the file as SCV and you can use this file with out any change (except remove the title line from it) for the second python code (a2256_table_final.py)

################################################################################

#################################### SECOND ####################################


INPUT for a2256_table_final.py:
===============================

1. File you got from SDSS that explained in FIRST part.

Result from a2256_table_final.py:
=================================

NOTE: you have to give the program the final table name that you chose.

It will give you the table that contanes these values for all galaxies :

'Radio Name(A),  Opt Name(B),  Ra Radio,  Dec Radio,  Area observed,  Area detectable, Area detectable (totF), '
  'flux(mJy),peak_flux(mJy),  log(lum)-W/Hz,  peak log(lum)-W/Hz  log(lum)-W/Hz-detectable,  num Opt,   num opt detectable,,num opt couldDect (totF),  Ra NED,  Dec NED,  z,  MagNED, objID SDSS,  '
  'Ra SDSS,  Dec SDSS,  Mag_u,  Mag_g,  Mag_r,  Mag_i,  Mag_z, fracDeV_u,  fracDeV_g,  fracDeV_r,  fracDeV_i,  fracDeV_z,  Note,  u-r'


all objects that has A number are radio galaxies and those that instead of radio properties, it is -1000000 are galaxies that are in the cluster but doesn't have radio counterpart.

