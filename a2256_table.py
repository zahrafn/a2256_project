#! /usr/bin/env python
import pyfits
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d 

######reading parameters from configuration file

file = open("config.txt", 'r')
content = file.read()
config = content.split("\n") #split it into lines
config_elem=[]
for c in config:
    elem=c.split("=")
    config_elem.append(float(elem[1]))
z=config_elem[0]
Image_size=config_elem[1]
ra_cent_deg=config_elem[2]
dec_cent_deg=config_elem[3]

########## Getting input file ##########
#### file should containe numbur, ra, dec, flux and peak_flux ina format of CSV

inRadio=raw_input('Enter RADIO poperties file "numbur, ra, dec, flux, peak_flux" : ')

info=open(inRadio,'r')
num= []
ra=[]
dec= []
flux= []
peak_flux=[]
for line in info:
  line = line.split(',')
  num.append(np.int(line[0]))
  ra.append(np.float(line[1]))
  dec.append(np.float(line[2]))
  flux.append(np.float(line[3]))
  peak_flux.append(np.float(line[4])) 
  
info.close()

num = np.array(num)
ra = np.array(ra)
dec = np.array(dec)
flux = np.array(flux)
peak_flux = np.array(peak_flux)


################# making changes from frazer (delete objects that are located farther than 20')
ra_cent_rad=ra_cent_deg*np.pi/180.
dec_cent_rad=dec_cent_deg*np.pi/180.
ra_rad_elim=[c*np.pi/180. for c in ra]
dec_rad_elim=[c*np.pi/180. for c in dec]

dist_from_center=[ (180.* 60./np.pi) * np.arccos(np.sin(d)*np.sin(dec_cent_rad)+np.cos(d)*np.cos(dec_cent_rad)*np.cos(r-ra_cent_rad)) for r,d in zip(ra_rad_elim,dec_rad_elim)]
dist_from_center = np.array(dist_from_center)
##############################################################################################

dist_ix = dist_from_center < 20.0
print "dist_ix ", dist_ix
num = num[dist_ix]
ra = ra[dist_ix]
dec = dec[dist_ix]
flux = flux[dist_ix]
peak_flux =peak_flux[dist_ix]


for e in dist_from_center:
  if e > 20.0:
    print e
####### getting rms file containing 

rmsANDr=raw_input('Enter RMS and raduis values  "radiuse valuse from the imag , rms:  ')


infoRmsANDr=open(rmsANDr, 'r')
imgr=[]
# I use 4.5 sigma instead of 5 sigma but didn't change the name of array for consistansy
fiverms=[]
for n in infoRmsANDr:
  n=n.split(",")
  imgr.append(np.float(n[0]))
  fiverms.append(np.float(n[1]))
infoRmsANDr.close()



#########
######### Finding fluxes in Jy

#flux is in microJy
r=[]
fluxJy=[(f*1.0e-6) for f in flux]
fluxJy=np.array(fluxJy)

peak_fluxJy=[(f*1.0e-6) for f in peak_flux]
peak_fluxJy=np.array(peak_fluxJy)



#z=0.058
dMpc=z*299792.458/70.
dm=dMpc*3.085677581e+22
Area=dm**2.*4.*np.pi
#dMpc=248.3995


############# Making two list numpy arrays
fiverms=np.array(fiverms)
imgr=np.array(imgr)

###### get rid of far values in plot to fit better
fiverms=np.delete(fiverms,6)
imgr=np.delete(imgr,6)

fiverms=np.delete(fiverms,13)
imgr=np.delete(imgr,13)

fiverms=np.delete(fiverms,16)
imgr=np.delete(imgr,16)


#####pixsize_image=8.333333333333E-05 
imgr=[r*8.333333333333*10**(-05)*np.pi/180.*dMpc for r in imgr] 

image_radi= Image_size*(np.pi/180.)*dMpc

intrpolt=interp1d(fiverms,imgr, bounds_error=False)
rDetect=intrpolt(peak_fluxJy)
rDetect_tot=intrpolt(fluxJy)


rDetect[peak_fluxJy>=fiverms.max()]=image_radi
rDetect[np.isnan(rDetect)] = 0

rDetect_tot[fluxJy>=fiverms.max()]=image_radi
rDetect_tot[np.isnan(rDetect_tot)] = 0


#rDetect=[r for r in rDetect if r>0.0]
Area_detectable=np.array([(i**2.*np.pi) for i in rDetect])
Area_detectable_tot=np.array([(i**2.*np.pi) for i in rDetect_tot])



#ra_cent=255.93146*np.pi/180.
#dec_cent=78.66662*np.pi/180.
ra_cent=ra_cent_deg*np.pi/180.
dec_cent=dec_cent_deg*np.pi/180.
ra_rad=[c*np.pi/180. for c in ra]
dec_rad=[c*np.pi/180. for c in dec]

dis=np.zeros(len(ra_rad))
Area_d=np.zeros(len(ra_rad))
for i in range(len(ra_rad)):
  dis[i]=(np.arccos(np.sin(dec_rad[i])*np.sin(dec_cent)+np.cos(dec_rad[i])*np.cos(dec_cent)*np.cos(ra_rad[i]-ra_cent)))*dMpc
  Area_d[i]=np.pi*dis[i]**2




  
#ra,dec,Area_d,Area_detectable,flux,dis=zip(*[(a,b,c,d,e,f) for a,b,c,d,e,f in zip(ra,dec,Area_d,
#  Area_detectable,flux,dis) if d!=0.000000])


F_SI= [(f*1e-26) for f in fluxJy]

lum=[(f*Area) for f in F_SI]
log_lum=[np.log10(l) for l in lum]

Peak_F_SI= [(f*1e-26) for f in peak_fluxJy]

peak_lum=[(f*Area) for f in Peak_F_SI]
peak_log_lum=[np.log10(l) for l in peak_lum]

###flux could been detected

dis_pix=[d/(dMpc*8.333333333333*10**(-05)*np.pi/180) for d in dis]

flux_couldObs=[5.51*10**(-19)*r**4-2.92*10**(-15)*r**3+7.22*10**(-12)*r**2-6.42*10**(-9)*r+2.22*10**(-5) for r in dis_pix]



Fcould_SI= [(f*1e-26) for f in flux_couldObs]

lum_could=[(f*Area) for f in Fcould_SI]
log_lum_could=[np.log10(l) for l in lum_could]

   

###### raading optica NED data ########
################### optical peoperties , should contain number, ra (opt),dec(opt)
inNED=raw_input('Enter OPTICAL poperties file "numbur, ra, dec, redshift, magnitude" : ')

infoNED=open(inNED,"r")
numNED=[]
RaNED=[]        
DecNED=[]
zNED=[]
MagNED=[]

##first line --> #,       Ra ,        Dec ,       redshift,       Mag/Qual Filter,   Note

for line in infoNED:
  line=line.strip().split(",")
  numNED.append(np.int(line[0]))
  RaNED.append(np.float(line[1]))
  DecNED.append(np.float(line[2]))
  zNED.append(np.float(line[3]))
  MagNED.append(str(line[4]))
  #NoteNED.append(str(line[5]))
  
infoNED.close()

numNED = np.array(numNED)
RaNED = np.array(RaNED)
DecNED = np.array(DecNED)
zNED = np.array(zNED)
MagNED = np.array(MagNED)






#NoteNED[56]="**Note:G12 No radio"
#NoteNED[140]="**Note G242: change optical position"
#TODO:
#RaNED[139]=256.2437 
#DecNED[139]=78.8330
##NoteNED[210]="**Note G310:optical position changed"
#RaNED[209]=256.6396
#DecNED[209]=78.4219


zNED_ix = zNED <= 0.068
zNED = zNED[zNED_ix]
numNED = numNED[zNED_ix]
RaNED = RaNED[zNED_ix]
DecNED = DecNED[zNED_ix]
MagNED = MagNED[zNED_ix]

zNED_ix = zNED >= 0.047
zNED = zNED[zNED_ix]
numNED = numNED[zNED_ix]
RaNED = RaNED[zNED_ix]
DecNED = DecNED[zNED_ix]
MagNED = MagNED[zNED_ix]



ra_rad_opt=[c*np.pi/180. for c in  RaNED]
dec_rad_opt=[c*np.pi/180. for c in DecNED]


dis_opt=np.zeros(len(ra_rad_opt))
dist_lim_opt=np.zeros(len(ra_rad_opt))
Area_d_opt=np.zeros(len(ra_rad_opt))

for i in range(len(ra_rad_opt)):
  dist_lim_opt[i] = np.arccos(np.sin(dec_rad_opt[i])*np.sin(dec_cent)+np.cos(dec_rad_opt[i])*np.cos(dec_cent)*np.cos(ra_rad_opt[i]-ra_cent))
  #dis_opt[i]=(np.arccos(np.sin(dec_rad_opt[i])*np.sin(dec_cent)+np.cos(dec_rad_opt[i])*np.cos(dec_cent)*np.cos(ra_rad_opt[i]-ra_cent)))*dMpc
  dis_opt[i] = dist_lim_opt[i] *dMpc
  Area_d_opt[i]=np.pi*dis_opt[i]**2 

################# deleting opticals that are farther than 20'
dist_from_center_opt=[ (180.* 60./np.pi) * d for d in dist_lim_opt]

for e in dist_from_center:
  if e > 20.0:
    print e

#################################################################


  
nums_opt=np.zeros(len(Area_d))
for i in range(len(Area_d)):
  a=0
  for o in Area_d_opt:
    if o<=Area_d[i]:
      a+=1
  nums_opt[i]=a


  

######### finiding the number of optical in area coulh have been detcted 
nums_opt_cdet=np.zeros(len(Area_detectable))
for i in range(len(Area_detectable)):
  a=0
  for o in Area_d_opt:
    if o<=Area_detectable[i]:
      a+=1
  nums_opt_cdet[i]=a

nums_opt_cdet_tot=np.zeros(len(Area_detectable_tot))
for i in range(len(Area_detectable_tot)):
  a=0
  for o in Area_d_opt:
    if o<=Area_detectable_tot[i]:
      a+=1
  nums_opt_cdet_tot[i]=a

#lum_could[i],nums_opt[i], nums_opt_cdet[i])


out_info_radio=open("out_radio_info_middle_1.txt","w")
for i in range(len(log_lum)):
  data_list=[ra[i],dec[i],flux[i],peak_flux[i],log_lum[i],peak_log_lum[i],Area_d[i],Area_detectable[i],Area_detectable_tot[i],log_lum_could[i],nums_opt[i], 
  nums_opt_cdet[i],nums_opt_cdet_tot[i]]
  data_list=[str(e) for e in data_list]
  out_info_radio.write("%s\n" % (',\t'.join(data_list)))

out_info_radio.close()



################### Second #######################

isNote=raw_input('Is there any notes for objects?(y/n) ')

if isNote=="y":
  noteFile=raw_input('Enter he Note file: : ')
  noteF=open(noteFile, 'r')
  namenote=[]
  note=[]
  for n in noteF:
    n=n.split(',')
    namenote.append(np.int(n[0]))
    note.append(np.str(n[1]))
  noteF.close()

NoteNED=[]
for i in range(len(numNED)):
  NoteNED.append("None")

if isNote=="y":
  for i in range(len(NoteNED)):
    for j in range(len(namenote)):
      if i==namenote[j]:
        NoteNED[i]= note[j]









infoRadio=open("out_radio_info_middle_1.txt","r")
RaRadio=[] 
DecRadio=[]
flux=[]
peak_flux=[]
log_lum=[]
peak_log_lum=[]
Area_obsr=[]
Area_detectable=[]
Area_detectable_tot=[]
log_lum_could=[]
num_opt=[]
num_opt_couldDect=[]
num_opt_couldDect_tot=[]


for line in infoRadio:
  line=line.strip().split(",")
  RaRadio.append(np.float(line[0]))
  DecRadio.append(np.float(line[1]))
  flux.append(np.float(line[2]))
  peak_flux.append(np.float(line[3]))
  log_lum.append(np.float(line[4]))
  peak_log_lum.append(np.float(line[5]))
  Area_obsr.append(np.float(line[6]))
  Area_detectable.append(np.float(line[7]))
  Area_detectable_tot.append(np.float(line[8]))
  log_lum_could.append(np.float(line[9]))
  num_opt.append(np.float(line[10]))
  num_opt_couldDect.append(np.float(line[11]))
  num_opt_couldDect_tot.append(np.float(line[12]))


infoRadio.close()

###### reading optical SDSS data########

###col0, objID, ra,  dec, run, rerun, camcol,  field, type,  modelMag_u,  modelMag_g,  modelMag_r,  modelMag_i,  modelMag_z,fracDeV_u, fracDeV_g, fracDeV_r,  fracDeV_i,  fracDeV_z



def toRadian(degree):
  radian=[d*np.pi/180.0 for d in degree]
  return radian

def dist(raT,decT,ra,dec):
  dis=[np.arccos(np.sin(d)*np.sin(decT)+np.cos(d)*np.cos(decT)*np.cos(r-raT)) for r,d in zip(ra,dec)]
  disn=[d*3600.*180./np.pi for d in dis]
  return disn
'''
def matchList(RaRadio_rad,DecRadio_rad,RaNED_rad,DecNED_rad):
  alldist_N=[]
  for i in range(len(RaRadio_rad)):
    raT=RaRadio_rad[i]
    decT=DecRadio_rad[i]
    alldist_N.append(dist(raT,decT,RaNED_rad,DecNED_rad))
  mins_NED=[]
  for e in alldist_N:
    e=np.array(e)
    e1=np.argmin(e)
    mins_NED.append(e1)
    return mins_NED
'''

#def find(dislist):
#    return np.where(dislist==min(dislist))

 

 ###### main ########
##### matching NED #####
RaRadio_rad=toRadian(RaRadio)
DecRadio_rad=toRadian(DecRadio)
RaNED_rad=toRadian(RaNED)
DecNED_rad=toRadian(DecNED)

#mins_NED=matchList(RaRadio_rad,DecRadio_rad,RaNED_rad,DecNED_rad)

  
alldist_N=[]
for i in range(len(RaRadio_rad)):
  raT=RaRadio_rad[i]
  decT=DecRadio_rad[i]
  alldist_N.append(dist(raT,decT,RaNED_rad,DecNED_rad))
mins_NED=[]
for e in alldist_N:
  e=np.array(e)
  e1=np.argmin(e)
  mins_NED.append(e1)
  print "e: ", np.amin(e)







########################

RaRadio=[str(r) for r in RaRadio]
DecRadio=[str(d) for d in DecRadio]
Area_obsr=[str(l) for l in Area_obsr]
Area_detectable=[str(a) for a in Area_detectable]
Area_detectable_tot=[str(a) for a in Area_detectable_tot]
flux=[str(f) for f in flux]
peak_flux=[str(f) for f in peak_flux]
log_lum=[str(l) for l in log_lum]
peak_log_lum=[str(l) for l in peak_log_lum]
log_lum_could=[str(l) for l in log_lum_could]
num_opt=[str(n) for n in num_opt]
num_opt_couldDect=[str(n) for n in num_opt_couldDect]
num_opt_couldDect_tot=[str(n) for n in num_opt_couldDect_tot]

RaNED=[str(r) for r in RaNED]
DecNED=[str(d) for d in DecNED]
zNED=[str(z) for z in zNED]



NoteNED=[e.strip() for e in NoteNED]



#######
outopt=open('outtable_onlyopts_middle_2.txt','w')
for i in range(len(RaRadio)):
  outopt.write('A'+str(i+1)+',  '+',  '.join([RaRadio[i], DecRadio[i],Area_obsr[i],Area_detectable[i],Area_detectable_tot[i],
    flux[i],peak_flux[i],log_lum[i],peak_log_lum[i],log_lum_could[i],num_opt[i],num_opt_couldDect[i],num_opt_couldDect_tot[i]])+',  '+'B'+str(i+1)+',   '+',   '.join([RaNED[mins_NED[i]] , DecNED[mins_NED[i]],zNED[mins_NED[i]], MagNED[mins_NED[i]], NoteNED[mins_NED[i]]])+'\n')

j=0
k=0
for i in range(len(RaNED)):
  if i not in mins_NED:
    j=j+1
    outopt.write('-1000000,  -1000000,  -1000000,  -1000000,  -1000000,  '
    '100000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000'+',  '+'B'+str(j+len(RaRadio))+',  '+',  '.join([RaNED[i] , DecNED[i],zNED[i], MagNED[i], NoteNED[i]])+'\n')


outopt.close()
######

###### reading again just to get ra and dec that already matched for SDSS List

intable=open('outtable_onlyopts_middle_2.txt','r')
RaNED_corr=[]
DecNED_corr=[]
zNED_corr=[]
MagNED_corr=[]
NoteNED_corr=[]

for line in intable:
  line=line.strip().split(',')
  RaNED_corr.append(np.str(line[15]))
  DecNED_corr.append(np.str(line[16]))
  zNED_corr.append(np.str(line[17]))
  MagNED_corr.append(np.str(line[18]))
  NoteNED_corr.append(np.str(line[19]))

intable.close()


outSDSS=open('SDSS_Inp.txt','w')
u=1
for a,b in zip(RaNED_corr,DecNED_corr):
  a=str(a)
  b=str(b)
  outSDSS.write('B'+str(u)+',  '+',  '.join([a, b])+'\n')
  u=u+1

outSDSS.close()
print "####### for SDSS"
u=1
for a,b in zip(RaNED_corr,DecNED_corr):
  print 'B',u,', ',a,', ',b
  u=u+1
print "####### for SDSS"








