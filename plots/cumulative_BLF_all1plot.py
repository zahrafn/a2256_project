#! /usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt




dtypes = [
    ('name_radio', 'S4'),
    ('name_opt_NED','S10'),
    ('ra_radio','float'),
    ('dec_radio','float'),
    ('area_observed','float'),
    ('area_detectable','float'),
    ('area_detectable_tot','float'),
    ('flux','float'),
    ('peak_flux','float'),
    ('loglum','float'),
    ('peak_loglum','float'), 
    ('loglum_deteable','float'),
    ('num_op','float'),
    ('num_opt_couldDect','float'),
    ('num_opt_couldDect_tot','float'),
    ('ra_opt_NED','float'),
    ('dec_opt_NED','float'),
    ('redshift','float'),
    ('mag_NED','S10'),
    ('objid_SDSS','int'),
    ('ra_opt_SDSS','float'),
    ('dec_opt_SDSS','float'),
    ('mag_u','float'),
    ('mag_g','float'),
    ('mag_r','float'),
    ('mag_i','float'),
    ('mag_z','float'),
    ('fracDev_u','float'),
    ('fracDev_g','float'),
    ('fracDev_r','float'),
    ('fracDev_i','float'),
    ('fracDev_z','float'),
    ('note','S40'),
    ('u-r','float')]


intable=raw_input('Enter info table : ')
 

table = np.genfromtxt(intable,delimiter=",",dtype=dtypes,
  usecols=(0,1,2,3,4,5,6,7,8,9,10,11,
            12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32))


#print table[:,np.array([0,1,2,3])]


#with open('table_May30.txt','r') as table:
#    lines = [tuple(line.split(",")) for line in table]



  
def toRadian(degree):
  return degree * np.pi/180.0

##### finding optical distances

z=0.058
dMpc=z*299792.458/70.
dpc=dMpc*10.**6
#print dpc
dm=dMpc*3.085677581e+22
Area=dm**2.*4.*np.pi


RaNED_rad=toRadian(table["ra_opt_NED"])
DecNED_rad=toRadian(table["dec_opt_NED"])

ra_cent=255.93146*np.pi/180.0
dec_cent=78.66662*np.pi/180.0
dis=[(np.arccos(np.sin(d)*np.sin(dec_cent)+np.cos(d)*np.cos(dec_cent)*
      np.cos(ra_cent-r)))*dMpc for r,d in zip(RaNED_rad,DecNED_rad)]


Area_observed_Ned=np.array([(i**2*np.pi) for i in dis])



#####



z=0.058
dMpc=z*299792.458/70.
dpc=dMpc*10.**6
#print dpc


names = ('mag_r', 'area_detectable','Area_observed', 'loglum', 'ra_opt_NED','dec_opt_NED')
i = (table['mag_r'] != -1000000) & (table['mag_r'] != -9999)
subtable = table[i]


###### finding absulute magnitudes ##############

mag_r_abs=np.array([m-5.*(np.log10(dpc)-1.) for m in subtable['mag_r']])


#####  sorting#####
i=mag_r_abs.argsort()

mag_r_abs=mag_r_abs[i]
subtable=subtable[i]
Area_observed_Ned=Area_observed_Ned[i]
########

############ 1 #############
t=subtable['loglum'].argsort()[::-1]
## sorted and reversed
mag_r_abs=mag_r_abs[t]
subtable=subtable[t]
Area_observed_Ned=Area_observed_Ned[t]



################ BLF ############
print "######## 1 ##########"

k=(-23.40<mag_r_abs) & (mag_r_abs<=-20.60)
mag_r_abs_1=mag_r_abs[k]
subtable_1=subtable[k]
Area_observed_Ned_1=Area_observed_Ned[k]

print "average 1: ", np.average(mag_r_abs_1)


t=subtable_1['loglum'].argsort()[::-1]
## sorted and reversed
mag_r_abs_1=mag_r_abs_1[t]
subtable_1=subtable_1[t]
Area_observed_Ned_1=Area_observed_Ned_1[t]




total_rate_1=[]
for i in range(len(subtable_1['area_detectable'])):
  count_opt=0.
  radio_opt=0.
  if subtable_1['area_detectable'][i]!=-1000000:
    for e in Area_observed_Ned_1:
      print i,' area ned: ',e, ', area detectable: ',subtable_1['area_detectable'][i]
      if e<=subtable_1['area_detectable'][i]:
        count_opt+=1.0
    radio_opt=1.0/count_opt
    total_rate_1.append(radio_opt)

err_each_1=[e**2 for e in total_rate_1]



total_1=[]
summ=0
for e in total_rate_1:
  summ+=e
  total_1.append(summ)

print total_rate_1

total_err_1=[]
summ_err_1=0
for e in err_each_1:
  summ_err_2_1=0
  summ_err_1+=e
  summ_err_2_1=summ_err_1**(1./2.)
  total_err_1.append(summ_err_2_1)

logtot_1=[np.log10(e) for e in total_1]

err_log_upper_1=[np.log10(n+e) for n,e in zip(total_1,total_err_1)]
err_logtot_upper_dif_1=[np.absolute(e-t) for e,t in zip(err_log_upper_1,logtot_1)]
err_log_lower_1=[np.log10(n-e) for n,e in zip(total_1,total_err_1)]
err_logtot_lower_dif_1=[np.absolute(e-t) for e,t in zip(err_log_lower_1,logtot_1)]



print "number of radio galaxies in range I of optical magnitude is: ", len(logtot_1)

loglum_1=[e for e in subtable_1['loglum'] if e!=-1000000]



############ 2 #############
print "######## 2 ##########"


j=(-20.60<mag_r_abs) & (mag_r_abs<=-19.55)
mag_r_abs_2=mag_r_abs[j]
subtable_2=subtable[j]
Area_observed_Ned_2=Area_observed_Ned[j]

print "average 2: ", np.average(mag_r_abs_2)

r=subtable_2['loglum'].argsort()[::-1]
mag_r_abs_2=mag_r_abs_2[r]
subtable_2=subtable_2[r]
Area_observed_Ned_2=Area_observed_Ned_2[r]


total_rate_2=[]
for i in range(len(subtable_2['area_detectable'])):
  count_opt=0.
  radio_opt=0.
  if subtable_2['area_detectable'][i]!=-1000000:
    for e in Area_observed_Ned_2:
      if e<=subtable_2['area_detectable'][i]:
        count_opt+=1
    radio_opt=1./count_opt
    total_rate_2.append(radio_opt)

err_each_2=[e**2 for e in total_rate_2]


print total_rate_2

total_2=[]
summ=0
for e in total_rate_2:
  summ+=e
  total_2.append(summ)

total_err_2=[]
summ_err_2=0
for e in err_each_2:
  summ_err_2_2=0
  summ_err_2+=e
  summ_err_2_2=summ_err_2**(1./2.)
  total_err_2.append(summ_err_2_2)

logtot_2=[np.log10(e) for e in total_2]


err_log_upper_2=[np.log10(n+e) for n,e in zip(total_2,total_err_2)]
err_logtot_upper_dif_2=[np.absolute(e-t) for e,t in zip(err_log_upper_2,logtot_2)]
err_log_lower_2=[np.log10(n-e) for n,e in zip(total_2,total_err_2)]
err_logtot_lower_dif_2=[np.absolute(e-t) for e,t in zip(err_log_lower_2,logtot_2)]

print "number of radio galaxies in range II of optical magnitude is: ", len(logtot_2)

loglum_2=[e for e in subtable_2['loglum'] if e!=-1000000]





############ 3 #############
print "######## 3 ##########"

h=(-19.55<mag_r_abs) & (mag_r_abs<=-12.10)
mag_r_abs_3=mag_r_abs[h]
subtable_3=subtable[h]
Area_observed_Ned_3=Area_observed_Ned[h]

print "average 3: ", np.average(mag_r_abs_3)

g=subtable_3['loglum'].argsort()[::-1]
mag_r_abs_3=mag_r_abs_3[g]
subtable_3=subtable_3[g]
Area_observed_Ned_3=Area_observed_Ned_3[g]


total_rate_3=[]
for i in range(len(subtable_3['area_detectable'])):
  count_opt=0.
  radio_opt=0.
  if subtable_3['area_detectable'][i]!=-1000000:
    for e in Area_observed_Ned_3:
      if e<=subtable_3['area_detectable'][i]:
        count_opt+=1
    radio_opt=1./count_opt
    total_rate_3.append(radio_opt)

err_each_3=[e**2 for e in total_rate_3]


total_3=[]
summ=0
for e in total_rate_3:
  summ+=e
  total_3.append(summ)

total_err_3=[]
summ_err_3=0
for e in err_each_3:
  summ_err_2_3=0
  summ_err_3+=e
  summ_err_2_3=summ_err_3**(1./2.)
  total_err_3.append(summ_err_2_3)

logtot_3=[np.log10(e) for e in total_3]


err_log_upper_3=[np.log10(n+e) for n,e in zip(total_3,total_err_3)]
err_logtot_upper_dif_3=[np.absolute(e-t) for e,t in zip(err_log_upper_3,logtot_3)]
err_log_lower_3=[np.log10(n-e) for n,e in zip(total_3,total_err_3)]
err_logtot_lower_dif_3=[np.absolute(e-t) for e,t in zip(err_log_lower_3,logtot_3)]


print "number of radio galaxies in range III of optical magnitude is: ", len(logtot_3)
    

print total_rate_3

loglum_3=[e for e in subtable_3['loglum'] if e!=-1000000]



p1=plt.plot(loglum_1,logtot_1,marker='o',linestyle='-',color='b')
p2=plt.plot(loglum_2,logtot_2,marker='^',linestyle='-',color='b')
p3=plt.plot(loglum_3,logtot_3,marker='s',linestyle='-',color='b')
#plt.legend([p1,p2,p3], [-21.2,-20.1,-18.8])
plt.legend()
plt.xlabel('logL(W/Hz)')
plt.ylabel('log(Fraction of Galaxies)')
plt.show()


print "######## end ##########"
 
total_E=0
total_S=0
for e in subtable['fracDev_r']:
  if e<0.6:
    total_S+=1
  else:
    total_E+=1

print "total_S: ", total_S
print "total_E: ", total_E

radio_S=0
radio_E=0
for i in range(84):
  if subtable['fracDev_r'][i]<0.6:
    radio_S+=1
  else:
    radio_E+=1

print 'radio_S: ',radio_S
print 'radio_E: ',radio_E                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            