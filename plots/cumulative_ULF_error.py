#! /usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt




dtypes = [
    ('name_radio', 'S4'),
    ('name_opt_NED','S10'),
    ('ra_radio','float'),
    ('dec_radio','float'),
    ('area_observed','float'),
    ('area_detectable','float'),
    ('area_detectable_tot','float'),
    ('flux','float'),
    ('peak_flux','float'),
    ('loglum','float'),
    ('peak_loglum','float'), 
    ('loglum_deteable','float'),
    ('num_op','float'),
    ('num_opt_couldDect','float'),
    ('num_opt_couldDect_tot','float'),
    ('ra_opt_NED','float'),
    ('dec_opt_NED','float'),
    ('redshift','float'),
    ('mag_NED','S10'),
    ('objid_SDSS','int'),
    ('ra_opt_SDSS','float'),
    ('dec_opt_SDSS','float'),
    ('mag_u','float'),
    ('mag_g','float'),
    ('mag_r','float'),
    ('mag_i','float'),
    ('mag_z','float'),
    ('fracDev_u','float'),
    ('fracDev_g','float'),
    ('fracDev_r','float'),
    ('fracDev_i','float'),
    ('fracDev_z','float'),
    ('note','S40'),
    ('u-r','float')]


intable=raw_input('Enter info table : ')
   
table = np.genfromtxt(intable,delimiter=",",dtype=dtypes,
  usecols=(0,1,2,3,4,5,6,7,8,9,10,11,
            12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32))


for a,b in zip(table['ra_opt_NED'],table['dec_opt_NED']):
  print a,', ',b
#print table[:,np.array([0,1,2,3])]


#with open('table_May30.txt','r') as table:
#    lines = [tuple(line.split(",")) for line in table]



  
def toRadian(degree):
  return degree * np.pi/180.0

##### finding optical distances

z=0.058
dMpc=z*299792.458/70.
dpc=dMpc*10.**6
#print dpc
dm=dMpc*3.085677581e+22
Area=dm**2.*4.*np.pi


RaNED_rad=toRadian(table["ra_opt_NED"])
DecNED_rad=toRadian(table["dec_opt_NED"])

ra_cent=255.93146*np.pi/180.0
dec_cent=78.66662*np.pi/180.0
dis=[(np.arccos(np.sin(d)*np.sin(dec_cent)+np.cos(d)*np.cos(dec_cent)*
      np.cos(ra_cent-r)))*dMpc for r,d in zip(RaNED_rad,DecNED_rad)]


Area_observed_Ned=np.array([(i**2*np.pi) for i in dis])



#####



z=0.058
dMpc=z*299792.458/70.
dpc=dMpc*10.**6
#print dpc


#names = ('mag_r', 'area_detectable','Area_observed', 'loglum', 'ra_opt_NED','dec_opt_NED')
#i = (table['mag_r'] != -1000000) & (table['mag_r'] != -9999)
subtable = table




###### finding absulute magnitudes ##############

mag_r_abs=np.array([m-5.*(np.log10(dpc)-1.) for m in subtable['mag_r']])


#####  sorting#####
i=mag_r_abs.argsort()

mag_r_abs=mag_r_abs[i]
subtable=subtable[i]
Area_observed_Ned=Area_observed_Ned[i]
########

############ 1 #############
t=subtable['loglum'].argsort()[::-1]
## sorted and reversed
mag_r_abs=mag_r_abs[t]
subtable=subtable[t]
Area_observed_Ned=Area_observed_Ned[t]



########## total fraction without dividing to two classes ######
total_rate=[]
for i in range(len(subtable['area_detectable_tot'])):
  count_opt=0.
  radio_opt=0.
  if subtable['area_detectable_tot'][i]!=-1000000:
    for e in Area_observed_Ned:
      if e<=subtable['area_detectable_tot'][i]:
        count_opt+=1.0
    radio_opt=1.0/count_opt
    print "count, ", count_opt 
    total_rate.append(radio_opt)


err_each=[e**2 for e in total_rate]

total=[]
summ=0
for e in total_rate:
  summ+=e
  total.append(summ)



logtot=[np.log10(e) for e in total]

total_err=[]
summ_err=0
for e in err_each:
  summ_err_2=0
  summ_err+=e
  summ_err_2=summ_err**(1./2.)
  total_err.append(summ_err_2)




err_log_upper=[np.log10(n+e) for n,e in zip(total,total_err)]
err_logtot_upper_dif=[np.absolute(e-t) for e,t in zip(err_log_upper,logtot)]
err_log_lower=[np.log10(n-e) for n,e in zip(total,total_err)]
err_logtot_lower_dif=[np.absolute(e-t) for e,t in zip(err_log_lower,logtot)]
err_logtot_lower_dif[0]=0.0

for a,b,c in zip(total,total_err,err_log_lower):
  print 'err_log_lower: ',a,', ',b, ', ',c
#print 'err_log_lower: ',err_log_lower ,', ', err_logtot_lower_dif
'''
i=0
for a,b in zip(total,total_err):
  i+=1
  print i,', ',a,', ',b
'''
#err_logtot_upper_dif=[np.absolute(e-t) for e,t in zip(total_err,total)]



#for a,b,c in zip(total_err,err_logtot_lower_dif,err_logtot_upper_dif):
#  print 'err, ',a,', ',b,',', c




########################################################

######### fractiones for two classes (spirals, others) #########
intable2=raw_input('Enter table from miller03(num, index from table, type) : ')


typ=open(intable2,'r')
index_table=[]
typee_gal_mil=[]
for line in typ:
  line = line.strip().split(',')
  index_table.append(np.float(line[1]))
  typee_gal_mil.append(np.str(line[2]))

for i,f in enumerate(subtable['fracDev_r']):
  for j,e in enumerate(index_table):
    if i==e:
      if typee_gal_mil[j]=='AGNo' or typee_gal_mil[j]=='b':
        print 'table ind AGN: ',i+1
        f=1.0
      elif typee_gal_mil[j]=='SF':
        print 'table ind SF: ', i+1
        f=0.0



area_detectable_spiral=[subtable['area_detectable_tot'][i] for i in  range(len(subtable['area_detectable_tot'])) if subtable['fracDev_r'][i]<0.6]
Area_observed_Ned_spiral=[Area_observed_Ned[i] for i in  range(len(Area_observed_Ned)) if subtable['fracDev_r'][i]<0.6]
area_detectable_other=[subtable['area_detectable_tot'][i] for i in  range(len(subtable['area_detectable_tot'])) if subtable['fracDev_r'][i]>=0.6]
Area_observed_Ned_other=[Area_observed_Ned[i] for i in  range(len(Area_observed_Ned)) if subtable['fracDev_r'][i]>=0.6]

print "len_spiral opt: ", len(Area_observed_Ned_spiral)
print "len_ellip opt: ", len(Area_observed_Ned_other)


total_rate_spiral=[]
for i in range(len(area_detectable_spiral)):
  count_opt_spiral=0.
  radio_opt_spiral=0.
  if area_detectable_spiral[i]!=-1000000:
    for e in Area_observed_Ned_spiral:
      if e<=area_detectable_spiral[i]:
        count_opt_spiral+=1.0
    radio_opt_spiral=1.0/count_opt_spiral
    total_rate_spiral.append(radio_opt_spiral)

total_rate_other=[]
total_rate_other=[]
for i in range(len(area_detectable_other)):
  count_opt_other=0.
  radio_opt_other=0.
  if area_detectable_other[i]!=-1000000:
    for e in Area_observed_Ned_other:
      if e<=area_detectable_other[i]:
        count_opt_other+=1.0
    radio_opt_other=1.0/count_opt_other
    total_rate_other.append(radio_opt_other)


err_each_s=[e**2 for e in total_rate_spiral]
err_each_o=[e**2 for e in total_rate_other]



total_err_s=[]
summ_err_s=0
for e in err_each_s:
  summ_err_2_s=0
  summ_err_s+=e
  summ_err_2_s=summ_err_s**(1./2.)
  total_err_s.append(summ_err_2_s)

total_err_o=[]
summ_err_o=0
for e in err_each_o:
  summ_err_2_o=0
  summ_err_o+=e
  summ_err_2_o=summ_err_o**(1./2.)
  total_err_o.append(summ_err_2_o)



#######################################
#print "len total_rate_spiral: ", len(total_rate_spiral)
#print "len total_rate_other: ", len(total_rate_other)



#print "number of radio galaxies in range I of optical magnitude is: ", len(logtot)

loglum=[e for e in subtable['loglum'] if e!=-1000000]


loglum_spiral=[]
#total_rate_spiral=[]
loglum_other=[]
#total_rate_other=[]



fracDev_r=[subtable['fracDev_r'][i] for i in range(len(loglum))]
for i,e in enumerate(fracDev_r):
  if e<0.6:
    loglum_spiral.append(loglum[i])
  else:
    loglum_other.append(loglum[i])




total_spiral=[]
summ_s=0
for e in total_rate_spiral:
  summ_s+=e
  total_spiral.append(summ_s)

logtot_spiral=[np.log10(e) for e in total_spiral]

err_log_upper_s=[np.log10(n+e) for n,e in zip(total_spiral,total_err_s)]
err_logtot_upper_dif_s=[np.absolute(e-t) for e,t in zip(err_log_upper_s,logtot_spiral)]
err_log_lower_s=[np.log10(n-e) for n,e in zip(total_spiral,total_err_s)]
err_logtot_lower_dif_s=[np.absolute(e-t) for e,t in zip(err_log_lower_s,logtot_spiral)]
err_logtot_lower_dif_s[0]=0.0

total_other=[]
summ_o=0
for e in total_rate_other:
  summ_o+=e
  total_other.append(summ_o)

logtot_other=[np.log10(e) for e in total_other]

err_log_upper_o=[np.log10(n+e) for n,e in zip(total_other,total_err_o)]
err_logtot_upper_dif_o=[np.absolute(e-t) for e,t in zip(err_log_upper_o,logtot_other)]
err_log_lower_o=[np.log10(n-e) for n,e in zip(total_other,total_err_o)]
err_logtot_lower_dif_o=[np.absolute(e-t) for e,t in zip(err_log_lower_o,logtot_other)]
err_logtot_lower_dif_o[0]=0.0

######### adding owen result to the plot ########

total_rate_owen_t=[4./2210.,14./2210.,31./2210.,30./2210.,29./2210.,41./2210.,26./1398.,7./334.,2./59.]
numerator_totate_owen=[4.,14.,31.,30.,29.,41.,26.,7.,2.]
denominator_totrate_owen=[2210.,2210.,2210.,2210.,2210.,2210.,1398.,334.,59.]
loglum_owen_t=[25.43,25.03,24.63,24.23,23.83,23.43,23.03,22.63,22.23]



total_ow_t=[]
summm_l=0
for e in total_rate_owen_t:
  summm_l+=e
  total_ow_t.append(summm_l)

#print 'total ow t:', total_ow_t

logtot_ow_t=[np.log10(e) for e in total_ow_t]


err_owen=[((a/b)**2)*(1./a+1./b) for a,b in zip(numerator_totate_owen,denominator_totrate_owen)]

total_err_ow=[]
summ_err_ow=0
for e in err_owen:
  summ_err_ow_2=0
  summ_err_ow+=e
  summ_err_ow_2=summ_err_ow**(1./2.)
  total_err_ow.append(summ_err_ow_2)

for a,b in zip(total_err_ow,total_ow_t):
  print 'err_owen: ',a,', ',b,', '

err_upper_owen=[np.log10(n+e) for n,e in zip(total_ow_t,total_err_ow)]
err_upper_owen_dif=[np.absolute(e-t) for e,t in zip(err_upper_owen,logtot_ow_t)]
err_lower_owen=[np.log10(n-e) for n,e in zip(total_ow_t,total_err_ow)]
err_lower_owen_dif=[np.absolute(e-t) for e,t in zip(err_lower_owen,logtot_ow_t)]
#err_lower_owen_dif[0]=0.0


#for a,b in zip(err_upper_owen_dif,err_lower_owen_dif):
#  print "down err: ", a, "down err: ", b

###################################################

'''
######### adding Coma(Miller) result to the plot ########
intable3=raw_input('Enter info table of coma cluster : ')

comainfo=open(intable3,'r')
log_lum_coma= []
inv_Area_coma=[]

for line in comainfo:
  line = line.split(',')
  log_lum_coma.append(np.float(line[0]))
  inv_Area_coma.append(np.float(line[1]))



bin=np.array([20.3,20.9,21.5,22.1,22.7,23.3,23.9])
countperea_coma=np.array([4.3,4.8,4.5,1.1,0.8,0.4,0.4])

bin_htol=bin[::-1]
countperea_coma_htol=countperea_coma[::-1]

summ_com=0.
total_coma=[]
for a in countperea_coma_htol:
  summ_com+=a
  total_coma.append(summ_com)

logtot_coma=[np.log10(e) for e in total_coma]

errM=np.array([1.55,1.1,1.1,0.75,0.67,0.4,0.4])

errM2=np.array(errM)
errM2[errM>=countperea_coma] = countperea_coma[errM>=countperea_coma]*.9999999

###############################################################
'''

#plt.plot(loglum,logtot,'r',marker='o',linestyle='--',color='blue')
#plt.plot(loglum_owen_t,logtot_ow_t,'b',marker='o',linestyle='--',label='Ledlow & Owen - 96', color='green')
#plt.plot(bin_htol,logtot_coma,'r',marker='o',linestyle='--',color='red')
p1=plt.errorbar(loglum,logtot,yerr=[err_logtot_lower_dif,err_logtot_upper_dif],marker='o',linestyle='--',color='black')
p2=plt.errorbar(loglum_owen_t,logtot_ow_t,yerr=[err_lower_owen_dif,err_upper_owen_dif],marker='o',linestyle='--',label='Ledlow & Owen - 96', color='green')

plt.legend([p1,p2], ['A2256','Ledlow et al. 1996'])
plt.xlabel('logL(W/Hz)')
plt.ylabel('log(Fraction of Galaxies)')
plt.show()


####example
#plt.errorbar(xrange(5), [2,5,3,4,7], yerr=[[1,4,2,3,6],[4,10,6,8,14]])
#plt.show()

############################### uni LF spiral vs ellip ################################
#plt.errorbar(loglum,logtot,yerr=[err_logtot_lower_dif,err_logtot_upper_dif],marker='o',linestyle='--',color='blue')

p3=plt.errorbar(loglum_spiral,logtot_spiral,yerr=[err_logtot_lower_dif_s,err_logtot_upper_dif_s],marker='o',linestyle='--',color='blue')
p4=plt.errorbar(loglum_other,logtot_other,yerr=[err_logtot_lower_dif_o,err_logtot_upper_dif_o],marker='o',linestyle='--',color='red')

plt.legend([p3,p4], ['spiral galaxies','elliptical galaxies'])
plt.xlabel('logL(W/Hz)')
plt.ylabel('log(Fraction of Galaxies)')
plt.show()