#! /usr/bin/env python
import pyfits
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

#####********58 has been deleted
intable=raw_input('Enter info table : ')

info=open(intable,'r')
num= []
ra=[]
dec= []
flux= []
Area_d=[]
log_lum=[]
Area_detectable=[]

for line in info:
  line = line.split(',')
  num.append(np.str(line[0]))
  ra.append(np.float(line[2]))
  dec.append(np.float(line[3]))
  flux.append(np.float(line[7]))
  Area_d.append(np.float(line[4]))
  log_lum.append(np.float(line[9]))
  Area_detectable.append(np.float(line[5]))
 


Area_d,log_lum,Area_detectable=zip(*[(x,y,z) for x,y,z in zip(Area_d,log_lum,Area_detectable) if z!=0.0])
inv_Area=[(1/a) for a in Area_detectable]

log_lum,inv_Area,Area_d= zip(*sorted(zip(log_lum,inv_Area,Area_d),key=lambda x:x[0]))
log_lum = list(log_lum)
log_lum.reverse()
inv_Area = list(inv_Area)
inv_Area.reverse()
Area_d= list(Area_d)
Area_d.reverse()
#print log_lum
#print inv_Area

  
A=np.zeros(len(inv_Area))
  
##### Comapre to coma###########
intable2=raw_input('Enter info table of coma cluster : ')

comainfo=open(intable2,'r')
log_lum_coma= []
inv_Area_coma=[]

for line in comainfo:
  line = line.split(',')
  log_lum_coma.append(np.float(line[0]))
  inv_Area_coma.append(np.float(line[1]))
  
A_coma=np.zeros(len(inv_Area_coma))

for i in range(len(inv_Area_coma)):
  A_coma[i] = np.sum(inv_Area_coma[:i]) + inv_Area_coma[i]


log_lum_a2256=[]
log_lum_a2256=[c for c in log_lum if c>20.6000]

log_lum_a2256,inv_Area,Area_d=zip(*[(x,y,z) for x,y,z in zip(log_lum_a2256,inv_Area,Area_d) if z<=2.54469])
#print(len(log_lum_a2256))

inv_Area_comafor2256=[]
while(len(inv_Area_comafor2256)<len(log_lum_a2256)):
  inv_Area_comafor2256.append(0.392975)

A_comafor2256=np.zeros(len(inv_Area_comafor2256))
for i in range(len(inv_Area_comafor2256)):
  A_comafor2256[i] = np.sum(inv_Area_comafor2256[:i]) + inv_Area_comafor2256[i]

#for i in range(len(A_comafor2256)):
#  print '%f    %f' % (log_lum_a2256[i],A_comafor2256[i])
  
#print("len: ",len(log_lum_a2256),len(log_lum_coma))

Err_2256=np.zeros(len(A_comafor2256))
i=0
while i<len(A_comafor2256):
  Err_2256[i]=np.sqrt(A_comafor2256[i]**2/(i+1))
  i+=1
  
Err_coma=np.zeros(len(A_coma))
i=0
while i<len(A_coma):
  Err_coma[i]=np.sqrt(A_coma[i]**2/(i+1))
  i+=1

Err_comaI=np.array(Err_coma)
Err_comaI[Err_coma>=A_coma] = A_coma[Err_coma>=A_coma]*.9999999   

Err_2256I=np.array(Err_2256)
Err_2256I[Err_2256>=A_comafor2256] = A_comafor2256[Err_2256>=A_comafor2256]*.9999999 

#p= plt.plot(log_lum_a2256,A_comafor2256,marker='o',linestyle='--', color='r', label='')
perr=plt.errorbar(log_lum_a2256,A_comafor2256,yerr=[Err_2256I,Err_2256],marker='o',linestyle='--',color='r')

#pc= plt.plot(log_lum_coma,A_coma,marker='o',linestyle='--', color='b', label='')
perragn=plt.errorbar(log_lum_coma,A_coma,yerr=[Err_comaI,Err_coma],marker='o',linestyle='--',color='b')

plt.legend([perr,perragn], ['A2256','Coma(Miller et al)'])
plt.text(20.7, 16.5, 'Only for galaxies within r=0.9 Mpc', ha='left')

plt.xlabel('logL(W/Hz)')
plt.ylabel('cum counts/Area (Mpc-2)')
plt.show()

exit()
