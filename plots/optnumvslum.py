import numpy as np
import matplotlib.pyplot as plt

intable=raw_input('Enter info table : ')

table=open(intable, 'r')

'''
Radio Name(A),  Opt Name(B),  Ra Radio,  Dec Radio,  Area observed,  Area detectable, Area detectable (totF), flux(mJy),peak_flux(mJy),  
log(lum)-W/Hz,  peak log(lum)-W/Hz  log(lum)-W/Hz-detectable,  num Opt,   num opt detectable,,num opt couldDect (totF),  Ra NED,  Dec NED,  z, 
 MagNED, objID SDSS,  Ra SDSS,  Dec SDSS,  Mag_u,  Mag_g,  Mag_r,  Mag_i,  Mag_z, fracDeV_u,  fracDeV_g,  fracDeV_r,  fracDeV_i,  fracDeV_z,  
Note,  u-r

'''

name_radio=[]
name_opt_NED=[] 
ra_radio=[]  
dec_radio=[]
area_observed=[]
area_detectable=[] 
area_detectable_tot=[] 
flux=[]
peak_flux=[]
loglum=[] 
peak_loglum=[]  
loglum_deteable=[] 
num_opt=[]
num_opt_couldDect=[]
num_opt_couldDect_tot=[]
ra_opt_NED=[]
dec_opt_NED=[] 
redshift=[]   
mag_NED=[] 
objid_SDSS=[]
ra_opt_SDSS=[]
dec_opt_SDSS=[] 
mag_u=[]
mag_g=[]
mag_r=[]
mag_i=[]
mag_z=[]
fracDev_u=[]
fracDev_g=[]
fracDev_r=[]
fracDev_i=[]
fracDev_z=[]
note=[]
u_r=[]





for line in table:
	line=line.split(",")
	name_radio.append(np.str(line[0]))
	name_opt_NED.append(np.str(line[1])) 
	ra_radio.append(np.float(line[2]))
	dec_radio.append(np.float(line[3]))
	area_observed.append(np.float(line[4]))
	area_detectable.append(np.float(line[5]))
	area_detectable_tot.append(np.float(line[6]))
	flux.append(np.float(line[7]))
	peak_flux.append(np.float(line[8]))
	loglum.append(np.float(line[9]))
	peak_loglum.append(np.float(line[10])) 
	loglum_deteable.append(np.float(line[11]))  
	num_opt.append(np.float(line[12]))
	num_opt_couldDect.append(np.float(line[13]))  
	num_opt_couldDect_tot.append(np.float(line[14]))  
	ra_opt_NED.append(np.float(line[15])) 
	dec_opt_NED.append(np.float(line[16]))  
	redshift.append(np.float(line[17]))  
	mag_NED.append(np.str(line[18]))   
	objid_SDSS.append(np.int(line[19]))
	ra_opt_SDSS.append(np.float(line[20]))
	dec_opt_SDSS.append(np.float(line[21]))
	mag_u.append(np.float(line[22]))
	mag_g.append(np.float(line[23]))
	mag_r.append(np.float(line[24]))
	mag_i.append(np.float(line[25]))
	mag_z.append(np.float(line[26]))
	fracDev_u.append(np.float(line[27]))
	fracDev_g.append(np.float(line[28]))
	fracDev_r.append(np.float(line[29]))
	fracDev_i.append(np.float(line[30]))
	fracDev_z.append(np.float(line[31]))
	note.append(np.str(line[32]))
	u_r.append(np.float(line[33]))



table.close()

print fracDev_r
print mag_r
print num_opt




z=0.058
dMpc=z*299792.458/70.
dpc=dMpc*10.**6
#print dpc
dm=dMpc*3.085677581e+22
Area=dm**2.*4.*np.pi


RaNED_rad=[e*np.pi/180.0 for e in ra_opt_NED]
DecNED_rad=[e*np.pi/180.0 for e in dec_opt_NED]

ra_cent=255.93146*np.pi/180.0
dec_cent=78.66662*np.pi/180.0
dis=[(np.arccos(np.sin(d)*np.sin(dec_cent)+np.cos(d)*np.cos(dec_cent)*
      np.cos(ra_cent-r)))*dMpc for r,d in zip(RaNED_rad,DecNED_rad)]


Area_observed_Ned=np.array([(i**2*np.pi) for i in dis])

count_list=[]
count=1.
for e in Area_observed_Ned:
	for a in Area_observed_Ned:
		if e<a:
			count+=1.
	count_list.append(count)





#for a,b,c in zip(name_opt_NED,ra_opt_NED,dec_opt_NED):
#	print a,', ',b,', ',c



#for e,a,b,c,d,h,f in zip(name_opt_NED ,ra_opt_NED,dec_opt_NED,ra_opt_SDSS,dec_opt_SDSS,ra_radio,dec_radio):
#	print e,"  Radio: ",h," ",f,"  NED: ",a," ",b,"  SDSS: ",c," ",d
fracDev_r_all=[]
num_opt_all=[]
mag_r_all=[]
for i in range(len(mag_r)):
	if mag_r[i]!= -1000000.0 and num_opt[i]!=-1000000.0 and fracDev_r[i]!=-1000000.0:
		fracDev_r_all.append(fracDev_r[i])
		num_opt_all.append(num_opt[i])
		mag_r_all.append(mag_r[i])

#for e in num_opt:
#	print 'mag_r ',e
#print "fracDev_r_all",fracDev_r_all


half_num_opt_couldDect=[e/2 for e in num_opt_couldDect]
half_num_opt_couldDect_tot=[e/2 for e in num_opt_couldDect_tot]

redshift=[redshift[i] for i in range(83)]
loglum=[loglum[i] for i in range(83)]
peak_loglum=[peak_loglum[i] for i in range(83)]
half_num_opt_couldDect=[half_num_opt_couldDect[i] for i in range(83)]
half_num_opt_couldDect_tot=[half_num_opt_couldDect_tot[i] for i in range(83)]
num_opt_couldDect=[num_opt_couldDect[i] for i in range(83)]
num_opt_couldDect_tot=[num_opt_couldDect_tot[i] for i in range(83)]
name_radio=[name_radio[i] for i in range(83)]
num_opt=[num_opt[i] for i in range(83)]
mag_u=[mag_u[i] for i in range(83)]
fracDev_r=[fracDev_r[i] for i in range(83)]
area_observed=[area_observed[i] for i in range(83)]
dist_observed=[(e/np.pi)**0.5 for e in area_observed]


intable2=raw_input('Enter table from miller03(num, index from table, type) : ')


typ=open(intable2,'r')

index_table=[]
typee_gal_mil=[]
for line in typ:
  line = line.strip().split(',')
  index_table.append(np.int(line[1]))
  typee_gal_mil.append(np.str(line[2]))

for i,f in enumerate(fracDev_r):
	for j,e in enumerate(index_table):
		if i==e:
			if typee_gal_mil[j]=='AGNo' or typee_gal_mil[j]=='b':
				f=1.0
			elif typee_gal_mil[j]=='SF':
				f=0.0


mag_r_all_s,fracDev_r_all_s,count_list_s=zip(*sorted(zip(mag_r_all,fracDev_r_all,count_list),key=lambda x:x[0]))
loglum_s,half_num_opt_couldDect_tot_s,num_opt_couldDect_tot_s,num_opt_s1,fracDev_r_s1,name_radio_s1=zip(*sorted(zip(loglum,half_num_opt_couldDect_tot,num_opt_couldDect_tot,num_opt,fracDev_r,name_radio),key=lambda x:x[0]))
peak_loglum_s,half_num_opt_couldDect_s,num_opt_couldDect_s,num_opt_s2,fracDev_r_s2,name_radio_s2=zip(*sorted(zip(peak_loglum,half_num_opt_couldDect,num_opt_couldDect,num_opt,fracDev_r,name_radio),key=lambda x:x[0]))

#for i,a in enumerate(num_opt):
#	print 'num',i , ', ', a




'''
fig=plt.figure()
ax=fig.add_subplot(111, projection='3d')

ax.scatter(loglum,num_opt_couldDect,redshift, marker='o', markersize=100,c='r')
ax.set_xlabel('log(Lum(W/Hz))')
ax.set_ylabel('num opt(detactable)')
ax.set_zlabel('redshift')
'''
#p1= plt.scatter(peak_loglum,num_opt, c=redshift ,vmin=0.035,vmax=0.066, s=35)
#plt.colorbar()

#i=0
#for a,b in zip(flux,peak_flux):
#	i+=1
#	if a<b:
#		print "WTF flux  ", i,', ',a,', ',b



######## peak lum  and using peak flux to find area detectable and num detectable ######################


peak_loglum_ellip=[]
loglum_ellip=[]
num_opt_ellip_s1=[]
num_opt_ellip_s2=[]
peak_loglum_spiral=[]
loglum_spiral=[]
num_opt_spiral_s1=[]
num_opt_spiral_s2=[]
name_radio_spiral_s1=[]
name_radio_spiral_s2=[]
name_radio_ellip_s1=[]
name_radio_ellip_s2=[]


for i,e in enumerate(fracDev_r_s1):
	if e<0.6:
		loglum_spiral.append(loglum_s[i])
		num_opt_spiral_s1.append(num_opt_s1[i])
		name_radio_spiral_s1.append(name_radio_s1[i])	
	else:
		loglum_ellip.append(loglum_s[i])
		num_opt_ellip_s1.append(num_opt_s1[i])
		name_radio_ellip_s1.append(name_radio_s1[i])
		

for i,e in enumerate(fracDev_r_s2):
	if e<0.6:
		peak_loglum_spiral.append(peak_loglum_s[i])
		num_opt_spiral_s2.append(num_opt_s2[i])
		name_radio_spiral_s2.append(name_radio_s2[i])
	else:
		peak_loglum_ellip.append(peak_loglum_s[i])
		num_opt_ellip_s2.append(num_opt_s2[i])
		name_radio_ellip_s2.append(name_radio_s2[i])


num_opt_all_spiral=[]
num_opt_all_ellip=[]
mag_r_all_spiral=[]
mag_r_all_ellip=[]

for i,e in enumerate(fracDev_r_all_s):
	if e<0.6:
		num_opt_all_spiral.append(count_list_s[i])
		mag_r_all_spiral.append(mag_r_all_s[i])
	else:
		num_opt_all_ellip.append(count_list_s[i])
		mag_r_all_ellip.append(mag_r_all_s[i])

'''
p1= plt.scatter(peak_loglum_ellip,num_opt_ellip_s2,marker='o', color='red')
p2= plt.scatter(peak_loglum_spiral,num_opt_spiral_s2,marker='o', color='blue')
p3=plt.plot(peak_loglum_s,num_opt_couldDect_s,color='red')
p4=plt.plot(peak_loglum_s,half_num_opt_couldDect_s,color='green')

plt.xlabel('log(Lum(W/Hz)_peak)')
plt.ylabel('Number of optical sources')

plt.show()
'''



######## tot lum  and using tot flux to find area detectable_tot and num detectable_tot ######################
'''
for i, txt in enumerate(name_radio_spiral_s1):
    plt.annotate(txt, (loglum_spiral[i],num_opt_spiral_s1[i]))

for i, txt in enumerate(name_radio_ellip_s1):
    plt.annotate(txt, (loglum_ellip[i],num_opt_ellip_s1[i]))
'''



p1= plt.scatter(loglum_ellip,num_opt_ellip_s1,marker='o', color='red')
p2= plt.scatter(loglum_spiral,num_opt_spiral_s1,marker='o', color='blue')
p3=plt.plot(loglum_s,num_opt_couldDect_tot_s,linestyle='-',color='green')
p4=plt.plot(loglum_s,half_num_opt_couldDect_tot_s,linestyle='--',color='green')

# 50 (60%) galaxies are above than green line, 33(40%) galaxy lower than green line out of 83


plt.legend([p1,p2,p3,p4], ['Elliptical','Spiral','detectable','half'])
plt.xlabel('log(Lum(W/Hz)_tot)')
plt.ylabel('Number of optical sources')
plt.show()

'''
hist, bins = np.histogram(num_opt_all, bins=10)
width =  (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.plot(center, hist, 'o',linestyle='-')

plt.xlabel('# detected')
plt.ylabel('Number')
plt.show()
'''
'''
p5= plt.scatter(mag_r_all_ellip,num_opt_all_ellip,marker='.', color='red')
p6= plt.scatter(mag_r_all_spiral,num_opt_all_spiral,marker='.', color='blue')

plt.xlabel('mag_r')
plt.ylabel('Number of optical sources')
plt.show()

'''

