changes needes for making plots:

1.Have to change observable area and number of could been detected for source 10

2.changed r manutude to 17.14 for source 9



######### BRLF ==> cumulative_BLF_all1plot.py ########################

This code give you the cumulative BRLF for 3 diffrent optical magnitude rage all in one plot

input : final table.txt from a2256_table_final.py



######### URLF with compares ==> cumulative_ULF_error.py ########################

This code give you the 2 URLF : 1. with compare to Miller et al resurt and 2. 2 separet URLF for elliptical and spiral galaxies using fracdive. We used classification result from Miller et al 2003 for this.

* you have to make a txt file of "num(from miller table),num of object from our table that matched with miller's source, type of the galaxy"

input : final table.txt from a2256_table_final.py
		index_type_fromtable_miller03.txt


######### URLF with compares ==> optnumvslum.py ########################

This code give you the Number of detected sources in a detected are vs Luminosity of radio galaxies.(figure 5.1 thesis)

input : final table.txt from a2256_table_final.py
		index_type_fromtable_miller03.txt


######### URLF with compares ==> URLF_coma.py ########################

This code give you the URLF of A2256 compare to coma cluster 

* you have to make a txt file contaning "log_lum_coma, inv_Area_coma" from Miller paper.

input :final table.txt from a2256_table_final.py
		comainfo.txt