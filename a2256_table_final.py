#! /usr/bin/env python
import pyfits
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d 

#################### SDSS information in a form of CSV all you get from +fracDev 
inSDSS=raw_input('Enter SDSS OPTICAL poperties file, CSV format form SDSS Object crossid +fracDev: ')

sdssinfo=open(inSDSS,"r")
nameSDSS=[] 
objid=[]
RaSDSS=[]
DecSDSS=[]
Mag_u=[]
Mag_g=[]
Mag_r=[]
Mag_i=[]
Mag_z=[]
fracDeV_u=[]
fracDeV_g=[]
fracDeV_r=[]
fracDeV_i=[]
fracDeV_z=[]
star_gal=[]



for line in sdssinfo:
  line=line.strip().split(',')
  nameSDSS.append(np.str(line[0]))
  objid.append(np.str(line[1]))
  RaSDSS.append(np.str(line[2]))
  DecSDSS.append(np.str(line[3]))
  star_gal.append(np.str(line[7]))
  Mag_u.append(np.str(line[9]))
  Mag_g.append(np.str(line[10]))
  Mag_r.append(np.str(line[11]))
  Mag_i.append(np.str(line[12]))
  Mag_z.append(np.str(line[13]))
  fracDeV_u.append(np.str(line[14]))
  fracDeV_g.append(np.str(line[15]))
  fracDeV_r.append(np.str(line[16]))
  fracDeV_i.append(np.str(line[17]))
  fracDeV_z.append(np.str(line[18]))

sdssinfo.close()


radio_table=open('outtable_onlyopts_middle_2.txt', 'r')

name_radio=[]
ra_radio=[]  
dec_radio=[]
area_observed=[]
area_detectable= []
area_detectable_tot=[]
flux=[]
peak_flux=[]
loglum=[]
peak_loglum=[]  
loglum_deteable=[] 
num_opt=[]
num_opt_couldDect=[]
num_opt_couldDect_tot=[]
name_optical=[]
RaNED_corr=[]
DecNED_corr=[]
zNED_corr=[]
MagNED_corr=[]
NoteNED_corr=[]





for line in radio_table:
  line=line.split(",")
  name_radio.append(np.str(line[0]).strip())
  ra_radio.append(np.float(line[1]))
  dec_radio.append(np.float(line[2]))
  area_observed.append(np.float(line[3]))
  area_detectable.append(np.float(line[4]))
  area_detectable_tot.append(np.float(line[5]))
  flux.append(np.float(line[6]))
  peak_flux.append(np.float(line[7]))
  loglum.append(np.float(line[8]))
  peak_loglum.append(np.float(line[9]))
  loglum_deteable.append(np.float(line[10]))  
  num_opt.append(np.float(line[11]))
  num_opt_couldDect.append(np.float(line[12]))
  num_opt_couldDect_tot.append(np.float(line[13]))
  name_optical.append(np.str(line[14]).strip())
  RaNED_corr.append(np.str(line[15]).strip())
  DecNED_corr.append(np.str(line[16]).strip())
  zNED_corr.append(np.str(line[17]).strip())
  MagNED_corr.append(np.str(line[18]).strip())
  NoteNED_corr.append(np.str(line[19]).strip())

radio_table.close()

#def toRadian(degree):
#  radian=[d*np.pi/180.0 for d in degree]
#  return radian

'''
nameNED=['B'+str(i+1) for i in range(260)]
nameNED.pop(91)
nameNED.pop(95)
nameNED.pop(130)
'''
nameNED=nameSDSS
#RaSDSS_rad=toRadian(RaSDSS)
#DecSDSS_rad=toRadian(DecSDSS)

print 'nameSDSS: ', len(objid)
print 'nameNED: ', len(nameNED)


outtable_con=open("outtable_continue_middle_3.txt", "w")
#outtable_con.write('name Opt(NED),  RaNED,  DecNED,  zNED,  MagNED,  NoteNED,  name Opt(SDSS),  objID SDSS,'
#  '  Ra SDSS, Dec SDSS, Mag_u,  Mag_g,  Mag_r,  Mag_i,  Mag_z'+'\n')

for i in range(len(nameNED)):
  outtable_con.write(',  '.join([nameNED[i],RaNED_corr[i],DecNED_corr[i],zNED_corr[i],MagNED_corr[i],NoteNED_corr[i],nameSDSS[i],objid[i],
    RaSDSS[i],DecSDSS[i],Mag_u[i],Mag_g[i],Mag_r[i],Mag_i[i],Mag_z[i],fracDeV_u[i],fracDeV_g[i],fracDeV_r[i],fracDeV_i[i],fracDeV_z[i]])+'\n')

#  else:
#    outtable_con.write(',  '.join([nameNED[i],RaNED_corr[i],DecNED_corr[i],zNED_corr[i],MagNED_corr[i],NoteNED_corr[i]])+',  -1000000,  -1000000,  -1000000,'
#      '  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000'+'\n')


outtable_con.close()









opt_table=open('outtable_continue_middle_3.txt','r')


name_opt_NED=[] 
ra_opt_NED=[]
dec_opt_NED=[]   
redshift=[]   
mag_NED=[]   
note=[]
name_opt_SDSS=[] 
objid_SDSS=[]
ra_opt_SDSS=[]
dec_opt_SDSS=[]
mag_u=[]
mag_g=[]
mag_r=[]
mag_i=[]
mag_z=[]
fracDeV_u=[]
fracDeV_g=[]
fracDeV_r=[]
fracDeV_i=[]
fracDeV_z=[]

for line in opt_table:
  line=line.split(",")
  name_opt_NED.append(np.str(line[0]))  
  ra_opt_NED.append(np.float(line[1])) 
  dec_opt_NED.append(np.float(line[2]))  
  redshift.append(np.float(line[3]))  
  mag_NED.append(np.str(line[4]))   
  note.append(np.str(line[5]))
  name_opt_SDSS.append(np.str(line[6])) 
  objid_SDSS.append(np.int(line[7]))
  ra_opt_SDSS.append(np.float(line[8]))
  dec_opt_SDSS.append(np.float(line[9]))
  mag_u.append(np.float(line[10]))
  mag_g.append(np.float(line[11]))
  mag_r.append(np.float(line[12]))
  mag_i.append(np.float(line[13]))
  mag_z.append(np.float(line[14]))
  fracDeV_u.append(np.float(line[15]))
  fracDeV_g.append(np.float(line[16]))
  fracDeV_r.append(np.float(line[17]))
  fracDeV_i.append(np.float(line[18]))
  fracDeV_z.append(np.float(line[19]))

opt_table.close()


u_r=[u-r for u,r in zip(mag_u,mag_r)]
color_morph=[]
for i in range(len(name_opt_NED)):
  if u_r[i]>=2.17:
    color_morph.append('A')
  elif u_r[i]<=1.35 and u_r[i]!=0.0:
    color_morph.append('S')
  else:
    color_morph.append('UN')


#for i in range(len(color_morph)):
# print i, mag_u[i], mag_r[i], u_r[i], color_morph[i]


note[16]='opt & radi not in the same position'
note[66]='opt & radi not in the same position'
note[72]='opt & radi not in the same position'
note[73]='opt & radi not in the same position'
note[74]='In the relic optical a little off'
note[63]='optical a little off'
note[60]='near the relic'
note[28]='no id in NED I added to NED list since it is very close to 27' 









name_radio=[str(r).strip() for r in name_radio]
ra_radio=[str(format(r,"20.5f")).strip() for r in ra_radio]  
dec_radio=[str(format(r,"20.5f")).strip() for r in dec_radio]
area_observed=[str(format(r,"20.5f")).strip() for r in area_observed]
area_detectable= [str(format(r,"20.5f")).strip() for r in area_detectable] 
area_detectable_tot= [str(format(r,"20.5f")).strip() for r in area_detectable_tot] 
flux=[str(r).strip() for r in flux]
peak_flux=[str(r).strip() for r in peak_flux]
loglum=[str(format(r,"20.3f")).strip() for r in loglum]
peak_loglum=[str(format(r,"20.3f")).strip() for r in peak_loglum]
loglum_deteable=[str(format(r,"20.3f")).strip() for r in loglum_deteable] 
num_opt=[str(r).strip() for r in num_opt]
num_opt_couldDect=[str(r).strip() for r in num_opt_couldDect]
num_opt_couldDect_tot=[str(r).strip() for r in num_opt_couldDect_tot]

name_opt_NED=[str(r).strip() for r in name_opt_NED]
ra_opt_NED=[str(format(r,"20.5f")).strip() for r in ra_opt_NED]
dec_opt_NED=[str(format(r,"20.5f")).strip() for r in dec_opt_NED]   
redshift=[str(format(r,"20.3f")).strip() for r in redshift]   
mag_NED=[str(r).strip() for r in mag_NED]   
note=[str(r).strip() for r in note]

name_opt_SDSS=[str(r).strip() for r in name_opt_SDSS] 
objid_SDSS=[str(r).strip() for r in objid_SDSS]
ra_opt_SDSS=[str(format(r,"20.5f")).strip() for r in ra_opt_SDSS]
dec_opt_SDSS=[str(format(r,"20.5f")).strip() for r in dec_opt_SDSS]
mag_u=[str(format(r,"20.2f")).strip() for r in mag_u]
mag_g=[str(format(r,"20.2f")).strip() for r in mag_g]
mag_r=[str(format(r,"20.2f")).strip() for r in mag_r]
mag_i=[str(format(r,"20.2f")).strip() for r in mag_i]
mag_z=[str(format(r,"20.2f")).strip() for r in mag_z]
fracDeV_u=[str(format(r,"20.5f")).strip() for r in fracDeV_u]
fracDeV_g=[str(format(r,"20.5f")).strip() for r in fracDeV_g]
fracDeV_r=[str(format(r,"20.5f")).strip() for r in fracDeV_r]
fracDeV_i=[str(format(r,"20.5f")).strip() for r in fracDeV_i]
fracDeV_z=[str(format(r,"20.5f")).strip() for r in fracDeV_z]
u_r=[str(format(r,"20.2f")).strip() for r in u_r]
#color_morph=[str(r).strip() for r in color_morph]
#radio_morph=[str(r).strip() for r in radio_morph]
#opt_morph=[str(r).strip() for r in opt_morph]
#final_morph=[str(r).strip() for r in final_morph]

tablename=raw_input('Enter name of final table : ')


#final_table=open('table_Auto1.txt','w')

final_table=open(tablename,'w')
final_table.write('Radio Name(A),  Opt Name(B),  Ra Radio,  Dec Radio,  Area observed,  Area detectable, Area detectable (totF), '
  'flux(mJy),peak_flux(mJy),  log(lum)-W/Hz,  peak log(lum)-W/Hz , log(lum)-W/Hz-detectable,  num Opt,   num opt detectable,,num opt couldDect (totF),  Ra NED,  Dec NED,  z,  MagNED, objID SDSS,  '
  'Ra SDSS,  Dec SDSS,  Mag_u,  Mag_g,  Mag_r,  Mag_i,  Mag_z, fracDeV_u,  fracDeV_g,  fracDeV_r,  fracDeV_i,  fracDeV_z,  Note,  u-r' +"\n")
j=0
for i in range(len(name_opt_NED)):
  if ra_radio[j]!='-1000000.00000':
    final_table.write(',  '.join([name_radio[j],name_opt_NED[j],ra_radio[j],dec_radio[j],
      area_observed[j],area_detectable[j],area_detectable_tot[j],flux[j],peak_flux[j], loglum[j],peak_loglum[j],loglum_deteable[j],num_opt[j],
      num_opt_couldDect[j],num_opt_couldDect_tot[j],ra_opt_NED[i],dec_opt_NED[i],redshift[i],mag_NED[i],objid_SDSS[i],
        ra_opt_SDSS[i],dec_opt_SDSS[i],mag_u[i],mag_g[i],mag_r[i],mag_i[i],mag_z[i],fracDeV_u[i],fracDeV_g[i],fracDeV_r[i],fracDeV_i[i],fracDeV_z[i],note[i],u_r[i]])+'\n')
  if ra_radio[j] =='-1000000.00000':
    final_table.write('-1000000,  '+name_opt_NED[i]+',  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  '
      '-1000000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  -1000000,  '+',  '.join([ra_opt_NED[i],
        dec_opt_NED[i],redshift[i],
        mag_NED[i],objid_SDSS[i],ra_opt_SDSS[i],dec_opt_SDSS[i],mag_u[i],mag_g[i],
        mag_r[i],mag_i[i],mag_z[i],fracDeV_u[i],fracDeV_g[i],fracDeV_r[i],fracDeV_i[i],fracDeV_z[i],note[i],u_r[i]])+'\n')
  j+=1
#print j
#print len(name_opt_NED)

print "########### radio positions ############"

print len(dec_radio)
print len(ra_radio)

for i in range(len(dec_radio)):
  print ra_radio[i],', ',dec_radio[i]

print "#########################################"

print "########### optical positions ############"

for j in range(len(dec_opt_NED)):
  print ra_opt_NED[j],', ',dec_opt_NED[j]

print "#########################################"

#for a,b,c,d in zip(name_opt_NED,ra_opt_NED,dec_opt_NED,opt_morph):
# print a,', ',b, ', ',c, ', ',d

final_table.close()




